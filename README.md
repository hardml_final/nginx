# nginx

Репозиторий с основной инфой: https://gitlab.com/hardml_final/deploy

### Как запустить nginx:
`ansible-playbook start_nginx.yml -i hosts.ini`

### Как переключить трафик:
`ansible-playbook switch_traffic.yml -i hosts.ini --extra-vars @config/blue.yml`

### Особенности
Предусмотрен fallback: если прод упал, пытаемся сходить за ответом в staging.
